# turnip-tv

**turnip-tv** is a tui _iptv_ client, written in Bash, for **Gnu/Linux and MacOS**.

![1read](screenshots/1read.png ){width=450}

turnip-tv can be costumized as far as the channel selecting program is concerned. The user can choose between:

- **read**
- **fzf**
- **rofi**
- **dmenu**

Through the main menu, the user can :

- Select directly  a  **Favorite Channel**.

- Select a **tag** to pick a channel from

![tag](screenshots/tag.png){width=450}

- Select a **Country** to pick a channel from

![country](screenshots/country.png)

- **Edit Channels** and their tags, by editing the specific file:

![edit_channels](screenshots/edit_channels.png){width=500}

- **Configure Preferences**, by editing the .conf file:

![nano](screenshots/nano.png){width=500}

- **❌ Quit turnip-tv**.


---
## Adding a Channel to turnip-tv

In order to **add a channel to turnip-tv**, the user edits the `$HOME/.config/turnip-tv/channels.txt` file, either within turnip-tv (**📋 Edit Channels**), or through any text editor.

**The format of the line should be the following**:

`channel-url  ~Name-of-the-Channel~ @#Country #Tag1 #Tag2 #Tag3`

for instance:

`https://live-01-01-tn.vodgc.net/TN24/index.m3u8 ~TN-Todo-Noticias~ @#Argentina #News #Favorites`

- The `channel-url` should go first: `https://lyd.nrk.no/nrk_radio_jazz_mp3_h`
- `Name of the Channel` should come next, between **tildes** `~`, and whitespaces substituted by **hyphen** `-` : `~TN-Todo-Noticias~`
  - `Country` name starts always with `@#`, and whitespaces are substituted by **hyphen** `-` : `@#Czech-Republic`
- Finally, tags come next, starting with **number sign** `#`. `#News #Favorites`

    There can be as many tags in a line as the user likes . 

     The **`#Favorites` tag** adds the channel to the **Favorites**.

- Adding empty lines to the file has no repercussions to the functionality. Separating lines to groups is also done only for demonstative purposes. To comment out a line, **add `//` at the beginning**.

---


## Configuring turnip-tv (Preferences option)

As mentioned above, selecting the `Preferences option` (or editing `$HOME/.config/turnip-tv/turnip-tv.conf` file using any text editor), the user can set the following preferences:

- **Preferred selector**. Acceptable values: `read`, `fzf`, `rofi`, `dmenu`.
- **Preferred editor**. Acceptable values can be `vim`, `nano`, `gedit`, `xed` or any other terminal or graphical text editor command **+ any flags**. For further info, visiting the respectable command `man` pages is  recommended.
- **fzf format, dmenu format, rofi format**: Here the `fzf`,  `dmenu` and `rofi` command string can be configured. If the user is not sure they know what they are doing, they are advised to **leave these variables alone**. At any rate, visiting the respectable command `man` page is **highly recommended**. Also, **pipe symbols** `|` are not to be ignored, they stand for the end of the string.
- **Prompt text**. Prompt text defines the **prompt text for fzf, rofi and dmenu.Pipe symbol** `|` is not to be ignored, stands for the end of the string.
- **Show mpv keybindings**. Acceptable values: yes no. This variable toggles the appearence of a little **mpv keybinding cheatsheet**:

![keybindings](screenshots/keybindings.png){width=250}


---


## Dependencies


The principal dependency is the almighty [mpv](https://mpv.io/)

**Debian based linux**:

```
sudo apt install mpv
```

**macOS**:

```
brew install mpv
```

**turnip-tv** can function with no other dependencies, however, the user can install [fzf](https://github.com/junegunn/fzf), [rofi](https://github.com/davatorium/rofi) or [dmenu](https://tools.suckless.org/dmenu/), according to their preference.

![1fzf](screenshots/1fzf.png){width=300}

![1rofi](screenshots/1rofi.png){width=500}

![1dmenu](screenshots/1dmenu.png){width=500}

----

## INSTALL

- Open a terminal window and run:

```
git clone https://gitlab.com/christosangel/turnip-tv/
```

- Change directory to `turnip-tv/`, make `install.sh` executable:

```
cd turnip-tv/

chmod +x install.sh
```

- Run:


```
./install.sh install

```
You are ready to go!

---

## Run

from any directory, run the command:

```
turnip-tv.sh
```
![play](screenshots/play.png){width=500}

The mpv player is controlled with the mpv defined keybindings.

---

The user can also create a launcher using the image in the `png/` directory:

![png](png/turnip-tv.png){width=100}

The turnip icon was created by [DinosoftLabs - Flaticon](https://www.flaticon.com/free-icons/turnip). It was slightly modified according to my taste.

This script was inspired by and is _almost identical_ with another project called [radion](https://gitlab.com/christosangel/radion), which is a _tui internet radio client_.

The channel list was found [here](https://raw.githubusercontent.com/Free-TV/IPTV/master/playlist.m3u8), and was modified to fit the script's functionality.

---
## Uninstall

- To uninstall, from the `turnip-tv/` directory, run: 

```
./install.sh uninstall
```
- To purge the dot files as well, run :

```
./install.sh purge
```

---
***Enjoy!***

---
>**Disclaimer:**

> No responsibility is taken regarding the channels, their content, status or whether they are operational or not. Their presence in the `channels.txt` is exclusively demonstrative, represent nobody's taste, nationality, affiliations or orientation, what is more the user is expected to populate this file with channels of their preference. No resposibility is taken regarding use of this software and copyright laws**.


---

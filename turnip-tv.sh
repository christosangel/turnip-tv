#! /bin/bash
# ╭────────╮╭────╮
# │ TURNIP ││ TV │
# ╰────────╯╰────╯
#A bash script written by Christos Angelopoulos, November 2023, under GPL v2



function keybindings ()
{
	echo -e "  ${B}╭─────┬──────────╮ ╭─────┬─────────────╮"
	echo -e "  ${B}│${M}  ␣  ${B}│${C}    Pause ${B}│ │${M}  f  ${B}│${C}  Fullscreen ${B}│"
	echo -e "  ${B}├─────┼──────────┤ ├─────┼─────────────┤"
	echo -e "  ${B}│${M} 9 0 ${B}│${C}   ↑↓ Vol ${B}│ │${M}  s  ${B}│${C}  Screenshot ${B}│"
	echo -e "  ${B}├─────┼──────────┤ ├─────┼─────────────┤"
	echo -e "  ${B}│${M}  m  ${B}│${C}     Mute ${B}│ │${M} 1 2 ${B}│${C}    Contrast ${B}│"
	echo -e "  ${B}├─────┼──────────┤ ├─────┼─────────────┤"
	echo -e "  ${B}│${M} ← → ${B}│${C} Skip 10\"${B} │ │${M} 3 4 ${B}│${C}  Brightness${B} │"
	echo -e "  ${B}├─────┼──────────┤ ├─────┼─────────────┤"
	echo -e "  ${B}│${M} ↑ ↓ ${B}│${C} Skip 60\"${B} │ │${M} 7 8 ${B}│${C}  Saturation${B} │"
	echo -e "  ${B}├─────┼──────────┤ ├─────┼─────────────┤"
	echo -e "  ${B}│${M} , . ${B}│${C}    Frame ${B}│ │${M}  q  ${B}│${R}        Quit ${B}│"
	echo -e "  ${B}╰─────┴──────────╯ ╰─────┴─────────────╯${M}"
}

function play_station ()
{
	clear
	print_logo
	if [[ $SHOW_MPV_KEYBINDINGS == "yes" ]]; then keybindings;fi
	echo -e "${B}STATION: ${Y}$STATION${n}"
	echo -e "${B}URL    : ${C}$STATION_URL${n}"
	mpv $STATION_URL
}

function print_logo ()
{
	echo -e "${B}   ╭────────╮╭────╮"
	echo -e "   │ ${Y}TURNIP ${B}││ ${C}TV ${B}│"
	echo -e "   ╰────────╯╰────╯${n}"
}

function load_config ()
{
	if [[ "$OSTYPE" == "darwin"* ]]; then
		URL_OPENER="open"
	else
		URL_OPENER="xdg-open"
	fi
	PREF_SELECTOR="$(grep 'Preferred_selector' $HOME/.config/turnip-tv/turnip-tv.conf|awk '{print $2}')";
	FZF_FORMAT="$(grep 'fzf_format' $HOME/.config/turnip-tv/turnip-tv.conf|sed 's/fzf_format//;s/|//')";
	ROFI_FORMAT="$(grep 'rofi_format' $HOME/.config/turnip-tv/turnip-tv.conf|sed 's/rofi_format//;s/|//')";
		DMENU_FORMAT="$(grep 'dmenu_format' $HOME/.config/turnip-tv/turnip-tv.conf|sed 's/dmenu_format//;s/|//')";
	PREF_EDITOR="$(grep 'Preferred_editor' $HOME/.config/turnip-tv/turnip-tv.conf|sed 's/Preferred_editor//')";
	if [[ $PREF_SELECTOR == "rofi" ]]
	then PREF_SELECTOR_STR="$ROFI_FORMAT"
	elif [[ $PREF_SELECTOR == "fzf" ]]
	then PREF_SELECTOR_STR="$FZF_FORMAT"
	elif [[ $PREF_SELECTOR == "dmenu" ]]
	then PREF_SELECTOR_STR=""$DMENU_FORMAT""
	fi;
	PROMPT_TEXT="$(grep 'Prompt_text' $HOME/.config/turnip-tv/turnip-tv.conf|sed 's/Prompt_text//;s/|//')";
	SHOW_MPV_KEYBINDINGS="$(grep 'Show_mpv_keybindings' $HOME/.config/turnip-tv/turnip-tv.conf|awk '{print $2}')";
	main
}

function read_select_station ()
{
	LOOP2=0
	ABORT=0
	while [[ $LOOP2 -eq 0 ]]
	do
	clear
		print_logo
		echo -e "${Y}$TAG${n}"

		LINE=1
		LINEMAX=$(cat /tmp/turnip-tv_select_tag.txt|wc -l)
		echo -e "${B}╭────────────────────────────────╮"
		while [[ $LINE -le $LINEMAX ]]
		do
			PRINT_LINE="│ ${M}$LINE ${C}$(head -$LINE /tmp/turnip-tv_select_tag.txt|tail +$LINE|awk '{print $2"                          "}'|sed 's/-/ /g;s/~//g')"
			echo -e "${PRINT_LINE:0:53}${B}│"
			((LINE++))
		done
		echo -e "├────────────────────────────────┤"
		echo -e "│${M} X${R} 👈 Go Back                   ${B}│"
		echo -e "│${M} Q${R} ❌ Quit                      ${B}│"
		echo -e "├────────────────────────────────┤"
		echo -e "│${G} Enter number for station:     ${B} │"
		echo -e "╰────────────────────────────────╯${M}"

		read  INDEX
		INDEX_NUM="$(echo $INDEX|sed 's/[[:cntrl:]]//g;s/[a-z]//g;s/[A-Z]//g;s/[[:punct:]]//g;s/ //g')"
		if [[ $INDEX == "Q" ]]||[[ $INDEX == "q" ]];then clear;exit
		elif [[ $INDEX == "X" ]]||[[ $INDEX == "x" ]];then LOOP2=1;ABORT=1
		elif [[ -n $INDEX_NUM ]]&&[[ $INDEX_NUM -gt 0 ]]&&[[ $INDEX_NUM -le $LINEMAX ]]&&[[ $ABORT -eq 0 ]]
		then
			STATION_URL="$(awk '{print NR" "$0}' /tmp/turnip-tv_select_tag.txt|grep -E "^$INDEX_NUM "|awk '{print $2}')"
			STATION="$(awk '{print NR" "$0}' /tmp/turnip-tv_select_tag.txt|grep -E "^$INDEX_NUM "|awk '{print $3}'|sed 's/-/ /g;s/~//g')"
			play_station
			LOOP2=1
		fi
	done
}

function read_select_tag ()
{
	LOOP1=0
	while [[ $LOOP1 -eq 0 ]]
	do
		clear
		print_logo
		TAGS=( $(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "#"|grep -v "@#"|grep -v "#Favorites"|sort|uniq|sed 's/#//g;s/$//g') )
		FAVS=( $(grep "#Favorites" $HOME/.config/turnip-tv/channels.txt|grep -v -E ^$|grep -v -E ^//|awk {'print $2'}|sed 's/~//g') )
		COUNTRIES=( $(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "@#"|sort|uniq|sed 's/@#//g;s/$//g') )
		t=0
		f=0
		c=0
		echo -e "   ${B}╭─────────────────────Favorites─╮"
		while [[ $f -lt ${#FAVS[@]} ]]
		do
		FAVLINE="   │  ${M} $((f+1))${Y} ${FAVS[f]//-/ }                                  "
		echo -e "${FAVLINE:0:55}${B}│"
			((f++))
	done
		echo -e "   ${B}├──────────────────────────Tags─┤"
		while [[ $t -lt ${#TAGS[@]} ]]
		do
			TAGLINE="   │  ${M} $((t+f+1))${C} ${TAGS[t]}                                  "
			echo -e "${TAGLINE:0:55}${B}│"
			((t++))
		done
		echo -e "   ├───────────────────────Actions─┤"
		echo -e "   │   ${M}A${G} ⭐ All Channels           ${B}│"
		echo -e "   │   ${M}E${G} 📋 Edit Channels          ${B}│"
		echo -e "   │   ${M}P${G} 🔧 Preferences            ${B}│"
		echo -e "   │   ${M}Q${R} ❌ Quit Turnip TV         ${B}│"
		echo -e "   ├─────────────────────Countries─┤"
		while [[ $c -lt ${#COUNTRIES[@]} ]]
		do
		COUNTRYLINE="   │  ${M} $((f+t+c+1))${C} ${COUNTRIES[c]//-/ }                                  "
		echo -e "${COUNTRYLINE:0:55}${B}│"
			((c++))
	done
		echo -e "   ├───────────────────────────────┤"

		echo -e "   │${G} Enter number/letter:          ${B}│"
		echo -e "   ╰───────────────────────────────╯${M}"
		read TAG_INDEX
		TAG_INDEX_NUM="$(echo $TAG_INDEX|sed 's/[[:cntrl:]]//g;s/[a-z]//g;s/[A-Z]//g;s/[[:punct:]]//g;s/ //g')"
		if [[ $TAG_INDEX == "E" ]]||[[ $TAG_INDEX == "e" ]];then eval $PREF_EDITOR $HOME/.config/turnip-tv/channels.txt
		elif [[ $TAG_INDEX == "P" ]]||[[ $TAG_INDEX == "p" ]];then eval $PREF_EDITOR $HOME/.config/turnip-tv/turnip-tv.conf;load_config
		elif [[ $TAG_INDEX == "Q" ]]||[[ $TAG_INDEX == "q" ]];then clear;exit
		elif [[ $TAG_INDEX == "A" ]]||[[ $TAG_INDEX == "a" ]]||[[ $TAG_INDEX == "" ]];then 	TAG=":";	echo -e "${Y}⭐ All Channels${n}";grep -v -E ^$ $HOME/.config/turnip-tv/channels.txt|grep -v -E ^// >/tmp/turnip-tv_select_tag.txt;LOOP1=1
		elif [[ -n $TAG_INDEX_NUM ]]&&[[ $TAG_INDEX_NUM -gt $f ]]&&[[ $TAG_INDEX_NUM -le $(($t+$f)) ]]
		then
			TAG="#${TAGS[$(($TAG_INDEX_NUM-1-$f))]}"
			grep $TAG $HOME/.config/turnip-tv/channels.txt >/tmp/turnip-tv_select_tag.txt
			LOOP1=1
		elif [[ -n $TAG_INDEX_NUM ]]&&[[ $TAG_INDEX_NUM -gt $(($f+$t)) ]]&&[[ $TAG_INDEX_NUM -le $(($f+$t+$c)) ]]
		then
			TAG="#${COUNTRIES[$(($TAG_INDEX_NUM-1-$f-$t))]}"
			grep $TAG $HOME/.config/turnip-tv/channels.txt >/tmp/turnip-tv_select_tag.txt
			LOOP1=1
		elif [[ -n $TAG_INDEX_NUM ]]&&[[ $TAG_INDEX_NUM -gt 0 ]]&&[[ $TAG_INDEX_NUM -le $f ]]
		then
			STATION=${FAVS[$(($TAG_INDEX_NUM-1))]}
			STATION_URL="$(grep ~${STATION// /-}~ $HOME/.config/turnip-tv/channels.txt|awk '{print $1}')"
			play_station
		fi
	done
}

function select_tag ()
{
	LOOP1=0
	while [[ $LOOP1 -eq 0 ]]
	do
	clear
		print_logo
		if [[ $PREF_SELECTOR == fzf ]]
		then
			TAG_INDEX="$(echo -e "${B}────────────────────── ⭐ Favorites\n${Y}$(grep "#Favorites" $HOME/.config/turnip-tv/channels.txt|grep -v -E ^$|grep -v -E ^//|awk {'print $2'}|sed 's/-/ /g;s/~//g')"${n}"\n${B}────────────────────── 🔖 Tags\n${C}$(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "#"|grep -v "@#"|grep -v "#Favorites"|sort|uniq|sed 's/#//g;s/$//g')${n}\n${B}────────────────────── 🔧 Actions\n${G}⭐ All Channels\n📋 Edit Channels\n🔧 Preferences\n${R}❌ Quit turnip-tv${n}\n${B}────────────────────── 🌐 Countries\n${C}$(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "@#"|sort|uniq|sed 's/@#//g;s/$//g')${n}"|eval "$PREF_SELECTOR_STR""\"$PROMPT_TEXT\"" )"
		elif [[ $PREF_SELECTOR == "dmenu" ]]||[[ $PREF_SELECTOR == "rofi" ]]
		then
			TAG_INDEX="$(echo -e "────────────────────── ⭐ Favorites\n$(grep "#Favorites" $HOME/.config/turnip-tv/channels.txt|grep -v -E ^$|grep -v -E ^//|awk {'print $2'}|sed 's/-/ /g;s/~//g')""\n────────────────────── 🔖 Tags\n$(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "#"|grep -v "@#"|grep -v "#Favorites"|sort|uniq|sed 's/#//g;s/$//g')\n────────────────────── 🔧 Actions\n⭐ All Channels\n📋 Edit Channels\n🔧 Preferences\n❌ Quit turnip-tv\n────────────────────── 🌐 Countries\n$(sed 's/ /\n/g' $HOME/.config/turnip-tv/channels.txt |grep "@#"|sort|uniq|sed 's/@#//g;s/$//g')"|eval "$PREF_SELECTOR_STR""\"$PROMPT_TEXT\"" )"
		fi
		if [[ $TAG_INDEX == "📋 Edit Channels" ]];then eval $PREF_EDITOR $HOME/.config/turnip-tv/channels.txt
		elif [[ $TAG_INDEX == "🔧 Preferences" ]];then eval $PREF_EDITOR $HOME/.config/turnip-tv/turnip-tv.conf;load_config
		elif [[ $TAG_INDEX == "❌ Quit turnip-tv" ]];then clear;exit
		elif [[ $TAG_INDEX == "⭐ All Channels" ]]||[[ $TAG_INDEX == "" ]];then 	TAG=":";	echo -e "${Y}⭐ All Channels${n}";LOOP1=1
		elif [[ $TAG_INDEX == *"────────────"* ]];then LOOP1=0
		#echo -e "${C}Congratulations.\nYou selected the separator line.\nA wise choise.\n${B}Press any key to continue.${n}";read -sn 1 d
		elif [[ -n $(grep "~${TAG_INDEX// /-}~" $HOME/.config/turnip-tv/channels.txt|awk '{print $1}') ]]
		then
		STATION=$TAG_INDEX
		STATION_URL="$(grep ~${STATION// /-}~ $HOME/.config/turnip-tv/channels.txt|awk '{print $1}')"
		play_station
		else TAG="#$TAG_INDEX";echo -e "${Y}$TAG${n}";LOOP1=1;fi
	done
}

function select_station ()
{
	STATION="$(echo -e "$(grep "$TAG" $HOME/.config/turnip-tv/channels.txt|grep -v -E ^$|grep -v -E ^//|awk {'print $2'}|sed 's/-/ /g;s/~//g')\n────────────────────────\n👈 Go Back\n❌ Quit turnip-tv"|eval "$PREF_SELECTOR_STR""\"Select $TAG Station \"")"
	if [[ $STATION == "❌ Quit turnip-tv" ]];then clear;exit;fi
	if [[ $STATION == "👈 Go Back" ]];then STATION="";fi
	if [[ -n $STATION ]]
	then
		STATION_URL="$(grep ~${STATION// /-}~ $HOME/.config/turnip-tv/channels.txt|awk '{print $1}')"
		play_station
	fi
}

function main ()
{
	while true
	do
		if [[ $PREF_SELECTOR == "read" ]]
		then
			read_select_tag
			read_select_station
		else
			select_tag
			select_station
		fi
		clear
	done
}
###############################################

B="\x1b[38;5;60m" #Grid Color
Y="\033[1;33m"    #Yellow
G="\033[1;32m"    #Green
I="\e[7m" #Invert
R="\033[1;31m"    #Red
M="\033[1;35m"    #Magenta
C="\033[1;36m"    #Cyan
n=`tput sgr0`     #Normal
export B Y G I R M C n #in order to work with fzf
load_config

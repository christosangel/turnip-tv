#!/usr/bin/env bash

preserve() {
  local SRC=$1
  local DST=$2
  test ! -f ${DST}/${SRC} && install -Dvcm644 -b ${SRC} -t ${DST}
}

uninstall() {
    rm -vf ~/.local/bin/turnip-tv.sh
    rm -vrf ~/.config/turnip-tv/png
}

case $1 in
  install)
    preserve channels.txt ~/.config/turnip-tv
    preserve turnip-tv.conf ~/.config/turnip-tv
    install -Dvcm755 turnip-tv.sh ~/.local/bin/
    install -Dvcm644 png/* -t ~/.config/turnip-tv/png
    ;;
  uninstall)
    uninstall
    ;;
  purge)
    uninstall
    rm -vrf ~/.config/turnip-tv
    ;;
  *)
    printf "USAGE: %s <command>\n" "$0"
    printf "   install    installs turnip-tv\n"
    printf "   uninstall  uninstalls the turnip-tv executable and assets\n"
    printf "   purge      completely uninstalls turnip-tv, including all turnip-tv config files\n"
    ;;
esac

exit 0
